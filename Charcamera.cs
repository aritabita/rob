﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Charcamera : MonoBehaviour
{
    //Defines the transform of the player object so the camera knows what to follow
    public Transform target;

    //The smaller the number the more "lag" the camera has with catching up with the player giving a nice effect
    //The higher the number the quicker the camera follows the player
    //I found that 0.1 is a good number to use for the speed that I think will give you the effect your looking for
    public float speed;

    //Used to make sure the camera is only moving in the x and y
    Vector3 desiredPosition;

    // Update is called once per frame
    void LateUpdate()
    {
        //sets the position to the target x and y while maintaining the cameras z position
        desiredPosition = new Vector3(target.position.x, target.position.y, transform.position.z);

        //updates the cameras position and speed as player moves
        transform.position = Vector3.Lerp(transform.position, desiredPosition, speed);
    }
}