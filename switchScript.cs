﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class switchScript : MonoBehaviour {

    public GameObject lightThing;
    private float timeroo = 0;
   
    private bool activated = false;
    public bool needHelp = false;
    public static switchScript switchInstance;

    // Use this for initialization
	void Start ()
    {
        switchInstance = this;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (timeroo > 0)
        {
            timeroo = timeroo - 1f * Time.deltaTime;
            activated = true;
            
        }
        else
        {
            lightThing.gameObject.SetActive(false);

            if (activated == true)
            {
               
                    
                    activated = false;
                    Debug.Log("unactive now");
                needHelp = true;
                

            } 
        }
	}

    void OnTriggerEnter2D(Collider2D switcheroo)
    {
        if (switcheroo.gameObject.tag == "Player")
        {
            lightThing.gameObject.SetActive(true);
            timeroo = 10f;
           
        }
    }

  
}
