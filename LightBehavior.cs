﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBehavior : MonoBehaviour {

    public float energygain = 1f;
    public float energyloss = -1f;


    // Use this for initialization
    void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D lightenter)
    {
        if (lightenter.gameObject.tag == "Player")
        {
            CharacterConroller.charinstance.energycontrolling = 0;
            Debug.Log("Player collided with light");

        }

    }

    void OnTriggerStay2D(Collider2D lightstay)
    {
        if (lightstay.gameObject.tag == "Player")
        {
            CharacterConroller.charinstance.energycontrolling = energygain;
            
        }

    }

    void OnTriggerExit2D(Collider2D lightexit)
    {
        if (lightexit.gameObject.tag == "Player")
        {
            CharacterConroller.charinstance.energycontrolling = energyloss;
            Debug.Log("Player is outside light");
        }


    }
}
