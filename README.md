## Introducing ROB, a Unity game that brings puzzles and platforms together!!

**Note: This project was finished on February 2017. The following repository is simply for portfolio reasons. As such, all assets except for the scripts are missing.**

Follow ROB as it wakes up in an abandoned factory, and finds itself to be the last of its kind. Determined, it attempts to leave the factory, only to notice a little problem: His batteries are terribly damaged! As a solar-powered robot though, ROB will be able to escape, so long as he doesn't let his batteries run out, and he's sure to stay as close to the sunlight as he can. Can he make it out of the factory...?

Features:

- A huge maze-like level.
- ROB is able to pick different tools that will help it on its journey to escape the factory.
- Power is constantly draining for ROB! As such, he must hurry to the source of light if he is to survive!

![Screenshot](https://cdn.discordapp.com/attachments/316438475098554368/456955925870149632/unknown.png)
