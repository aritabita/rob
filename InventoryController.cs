﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour {

    public GameObject crowbarMiniIcon;
    public GameObject crowbarUiIcon;
    public GameObject gearMiniIcon;
    public GameObject gearUiIcon;
    public GameObject gears;
    public int moveSpeed = 1;
    public bool isCrowbar = false;
    public bool isGear = false;
    private bool obtaining = false;
    
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (obtaining)
        {
            if ((Input.GetKey(KeyCode.F)))
            {
                if (isCrowbar)
                {
                    crowbarUiIcon.SetActive(true);
                    CharacterConroller.charinstance.crowbar = true;
                    StartCoroutine(floatingImagine());
                }

                if (isGear)
                {
                    gearUiIcon.SetActive(true);
                    CharacterConroller.charinstance.gear = true;
                    StartCoroutine(floatingImagine());
                }
            }

        }	
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if (isCrowbar || isGear)
        {
            gears.SetActive(true);
            obtaining = true;
           
        }
        
    }

    void OnTriggerExit2D(Collider2D col)
    {
        gears.SetActive(false);
        obtaining = false;

    }

    IEnumerator floatingImagine()
    {
        if (isCrowbar)
        {
            gears.SetActive(false);
            crowbarMiniIcon.SetActive(true);
            CharacterConroller.eventhappening = true;
            yield return new WaitForSecondsRealtime(1f);
            CharacterConroller.eventhappening = false;
            Destroy(gameObject);
        }

        if (isGear)
        {
            gears.SetActive(false);
            gearMiniIcon.SetActive(true);
            CharacterConroller.eventhappening = true;
            yield return new WaitForSecondsRealtime(1f);
            CharacterConroller.eventhappening = false;
            Destroy(gameObject);
        }

    }
}
