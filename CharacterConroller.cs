﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterConroller : MonoBehaviour {


    public float speed = 2.0f;
    public float jump = 20.0f;
    public bool isOnGround = false;
    private Rigidbody2D characterRigidBody;
    private SpriteRenderer sprited;
    public float energy = 50;
    public Text energyCounter;
    public float energycontrolling = 0;
    public static CharacterConroller charinstance;
    private int valuestorage;
    public Text Gameover;
    public bool dead = false;
    public float energygain = 1f;
    public float energyloss = -1f;
    private bool helpingOut = false;
    public float pushback = 10;
    private bool isfacingRight;
    public float damagetimer;
    public float timerahoy = 0.1f;
    public static bool eventhappening = false;
    public bool crowbar = false;
    public bool gear = false;
    public Animator robanim;
    public AudioSource ambient;
    public AudioSource lightsound;
    public AudioClip[] footsteps;
    public AudioClip jumpstart;
    public AudioClip jumpsend;
    public Light lifeLight;
    float alpha;
    public GameObject sr;

    // Use this for initialization
    void Start ()
    {

        charinstance = this;

        this.characterRigidBody = this.GetComponent<Rigidbody2D>();
        SetEnergyText();
        this.sprited = this.GetComponent<SpriteRenderer>();
        robanim = (Animator)GetComponent(typeof(Animator));
      

	}
	
	// Update is called once per frame
	void Update ()
    {
        CharacterMover();
        LifeMeter();
        Energyandstuff();
       


    }

    void FixedUpdate()
    {
        
       
        


    }
    
    void CharacterMover()
    {
        if (eventhappening == false)
        {

            if (damagetimer <= 0)
            {

                if (Input.GetKey(KeyCode.D))
                {
                    transform.Translate(Vector3.right * speed * Time.deltaTime);
                    isfacingRight = true;
                    
                    transform.localScale = new Vector3(0.1072603f, 0.1072603f, 0.1072603f);
                    robanim.SetBool("moving", true);

                }

                if (Input.GetKey(KeyCode.A))
                {
                    transform.Translate(Vector3.left * speed * Time.deltaTime);
                    isfacingRight = false;
                    
                    transform.localScale = new Vector3(-0.1072603f, 0.1072603f, 0.1072603f);
                    robanim.SetBool("moving", true);

                }

                if (Input.GetKey(KeyCode.Space))
                {
                    if (isOnGround == true && characterRigidBody.velocity.y == 0)
                    {
                        this.characterRigidBody.AddForce(new Vector2(0, jump), ForceMode2D.Impulse);
                        isOnGround = false;
                        robanim.SetBool("isJumping", true);

                    }


                }

                if (Input.GetKeyUp(KeyCode.D) || (Input.GetKeyUp(KeyCode.A)))
                {
                    robanim.SetBool("moving", false);
                }

            }
            else
            {
                if (isfacingRight)
                {

                    GetComponent<Rigidbody2D>().velocity = new Vector2(-pushback, 0);
                    damagetimer -= Time.deltaTime;
                    StartCoroutine(damaged());
                }

                else
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(pushback, 0);

                    damagetimer -= Time.deltaTime;
                    StartCoroutine(damaged());
                }
            }


        }
    }

    void OnCollisionEnter2D(Collision2D touchingSomething)
    {
        if (touchingSomething.gameObject.tag == "Ground")
        {
            isOnGround = true;
            robanim.SetBool("isJumping", false);
        }

       

    }

    void OnTriggerEnter2D(Collider2D lightenter)
    {
        if (lightenter.gameObject.tag == "Light")
        {
            energycontrolling = 0;
            Debug.Log("Player collided with light");
            StartCoroutine(fadeSoundAmbient());
            StartCoroutine(playSoundLight());

        }

        if (lightenter.gameObject.tag == "Steam")
        {
            Debug.Log("Steamy");
            damagetimer = 0.5f;
            energy = energy - 20;

        }

        if (lightenter.gameObject.tag == "Killplane")
        {
            energy = 0;
        }


    }

    void OnTriggerStay2D(Collider2D lightstay)
    {
        if (lightstay.gameObject.tag == "Light")
        {
            
                energycontrolling = energygain;
            
        }


    }

    void OnTriggerExit2D(Collider2D lightexit)
    {
        if (lightexit.gameObject.tag == "Light")
        {
            energycontrolling = energyloss;
            Debug.Log("Player is outside light");
            StartCoroutine(fadeSoundLight());
            StartCoroutine(playSoundAmbient());
        }


    }


    void Energyandstuff()
    {
        if (energy <= 100 && energycontrolling > 0)
        {
           
                energy = energy + 10f * Time.deltaTime;
           
        }


        if (energy <= 0)
        {
            Debug.Log("Energy is out");
            StartCoroutine(death());
        }

        if (energycontrolling < 0)
        {
            
            energy = energy - 1.6f * Time.deltaTime;
        }


       

    }
 

    void SetEnergyText()
    {
        valuestorage = (Mathf.RoundToInt(energy));
        energyCounter.text = valuestorage.ToString();
    }


    IEnumerator damaged()
    {
        sprited.enabled = false;
        yield return new WaitForSecondsRealtime(0.1f);
        sprited.enabled = true;
        yield return new WaitForSecondsRealtime(0.1f);
        sprited.enabled = false;
        yield return new WaitForSecondsRealtime(0.1f);
        sprited.enabled = true;
        yield return new WaitForSecondsRealtime(0.1f);
        sprited.enabled = false;
        yield return new WaitForSecondsRealtime(0.1f);
        sprited.enabled = true;
    }

    IEnumerator fadeSoundAmbient()
    {
        while (ambient.volume > 0.01f)
        {
            ambient.volume -= Time.deltaTime / 1.0f;
            yield return null;
        }

        ambient.volume = 0;
        ambient.Stop();
    }

    IEnumerator fadeSoundLight()
    {
        while (lightsound.volume > 0.01f)
        {
            lightsound.volume -= Time.deltaTime / 1.0f;
            yield return null;
        }

        lightsound.volume = 0;
        lightsound.Stop();
    }

    IEnumerator playSoundAmbient()
    {
        ambient.Play();

        while (ambient.volume < 1.0f)
        {
            ambient.volume += Time.deltaTime / 1.0f;
            yield return null;
        }



    }

    IEnumerator playSoundLight()
    {
        lightsound.Play();

        while (lightsound.volume < 1.0f)
        {
            lightsound.volume += Time.deltaTime / 1.0f;
            yield return null;
        }



    }

    public void PlaySound()
    {
        int rand = Random.Range(0, footsteps.Length);
        AudioSource.PlayClipAtPoint(footsteps[rand], Camera.main.transform.position);
    }

    public void PlaySoundJumpStart()
    {
       
        AudioSource.PlayClipAtPoint(jumpstart, Camera.main.transform.position);
    }

    public void PlaySoundJumpEnd()
    {
        AudioSource.PlayClipAtPoint(jumpsend, Camera.main.transform.position);
    }

    public void LifeMeter()
    {
        if (energy >= 100)
        {
            lifeLight.color = Color.white;
        }

        if (energy >= 50 && energy < 100)
        {
            lifeLight.color = Color.green;

        }

        if (energy >= 20 && energy < 50)
        {
            lifeLight.color = Color.yellow;
        }

        if (energy < 20)
        {
            lifeLight.color = Color.red;
        }

        
    }

    IEnumerator death()
    {
        eventhappening = true;
        sr.SetActive(true);
        robanim.SetBool("isDead", true);
        lifeLight.enabled = false;
        
        yield return new WaitForSecondsRealtime(2.5f);
        SceneManager.LoadScene("Title");
    }
}







