﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fader : MonoBehaviour {

    public RawImage im;
    public Text tx;
    public Text tx2;
    public AudioClip menu;
    

	// Use this for initialization
	void Start ()
    {

      
        im.canvasRenderer.SetAlpha(0.0f);
        tx.canvasRenderer.SetAlpha(0.0f);
        tx2.canvasRenderer.SetAlpha(0.0f);
        StartCoroutine(Fadein());
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.Return))
        {
            StopAllCoroutines();
            im.CrossFadeAlpha(0f, 3, false);
            tx.CrossFadeAlpha(0f, 3, false);
            tx2.CrossFadeAlpha(0f, 3, false);
            AudioSource.PlayClipAtPoint(menu, Camera.main.transform.position);
            StartCoroutine(scenechanger());
            Debug.Log("G pressed");
        }
        if (Input.GetKey(KeyCode.Backspace))
        {
            Application.Quit();
            Debug.Log("Y pressed");
        }
    }

   IEnumerator Fadein()
    {

        yield return new WaitForSecondsRealtime(3f);
            im.CrossFadeAlpha(1.0f, 3, false);
            tx.CrossFadeAlpha(1.0f, 3, false);
            tx2.CrossFadeAlpha(1.0f, 3, false);
        StartCoroutine(repeatingfade());
        
    }

    IEnumerator repeatingfade()
    {
        yield return new WaitForSecondsRealtime(1f);
       
        tx.CrossFadeAlpha(0.5f, 1, false);
        
        yield return new WaitForSecondsRealtime(1f);
        
        tx.CrossFadeAlpha(1f, 1, false);
        
        StartCoroutine(repeatingfade());
    }

    IEnumerator scenechanger()
    {
        yield return new WaitForSecondsRealtime(3f);
        SceneManager.LoadScene("Level");

    }


}
