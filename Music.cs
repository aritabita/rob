﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {

    public AudioClip menu;
    public Camera cam;
    
    // Use this for initialization
	void Start ()
    {
        GetComponent<Camera>();
        AudioSource.PlayClipAtPoint(menu, cam.transform.position);	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
