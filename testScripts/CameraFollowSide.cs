﻿using UnityEngine;
using System.Collections;

public class CameraFollowSide : MonoBehaviour
{
    // How far char can move away from camera before it follows
    public float xCharRange = 1.0f;
    
    //Smoothing of Camera Movement
    public float xTrackSmooth       = 10.0f;


    //Boundaries our camera can move in X axis
    public Vector2 maxXLevel;
    public Vector2 minXLevel;

    //Hold our camera track point position
    public Transform CameraTrackPoint;
 
     void FixedUpdate ()
     {

         TrackPlayer();
     }

    //getting distance of character away from camera if passed allowed range
     bool CheckXCharRange ()
     {

         return Mathf.Abs(transform.position.x - CameraTrackPoint.position.x) > (xCharRange);               
     }

    //Camera Tracking Main

    void TrackPlayer ()
    {
        //Var to store the positional information for game camera
        float targetX = transform.position.x;

        //if character is out of range, them move camera to follow player until in range again
        if (CheckXCharRange() )
        {

            targetX = Mathf.Lerp(transform.position.x, CameraTrackPoint.position.x, xTrackSmooth * Time.deltaTime);
        }

        //Clamp our camera range of movement
        targetX = Mathf.Clamp(targetX, minXLevel.x, maxXLevel.x);

        //Move our camera after calculations are done
        transform.position = new Vector3(targetX, transform.position.y, transform.position.z);


    }



 
}
