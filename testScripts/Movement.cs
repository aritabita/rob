﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
	public float maxSpeed = 10.0f; // Maximum Speed character can move.
	private bool facingRight = true; //When player is pressing the A key, this will be set to False
    public bool isGrounded;
	private Animator anim; //looks for controller

	void Start()
	{
		anim = GetComponent<Animator>(); //gets the current animator component attached to the game object (character)
        isGrounded = false;
	}

	void FixedUpdate()
	{
		float move = Input.GetAxis("Horizontal");  //assigns a variable to the Horizontal movment of the character to be used later
		anim.SetFloat("speed", Mathf.Abs(move)); //sets a value to our speed parameter that we created earlier
		GetComponent<Rigidbody2D>().velocity = new Vector2 (move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y); //sets the velocity of the character
		
		//IF player is moving and NOT facing right it runs the FlipFacing function below
		//IF player is moving and IS facing right it runs the FlipFacing function below
		if(move > 0 && !facingRight)
		{
			FlipFacing();
		}
		else if(move < 0 && facingRight)
		{
			FlipFacing();
		}

        if (Input.GetKeyDown(KeyCode.W) && isGrounded == true)
        {
            Jump();
        }
	}
	
	void FlipFacing()
	{
		facingRight = !facingRight; //toggles true/false

		//mirrors sprite
		Vector3 charScale = transform.localScale;
		charScale.x *= -1;
		transform.localScale = charScale;
	}

    public void Jump ()
    {
            //Debug.Log("I jumped!");
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 500f);﻿
            anim.SetTrigger("Jumped");  
        
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }
    }

    void OnCollisionStay2D (Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }

    }

    void OnCollisionExit2D (Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = false;
        }
    }
}