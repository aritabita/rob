﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorScript : MonoBehaviour {

    public GameObject player;
    public GameObject targetDoor;
    public GameObject gears;
    public GameObject blackScreen;
    public GameObject crowbarNeededMessage;
    public GameObject gearNeededMessage;

    public bool needsCrowbar = false;
    public bool needsGear = false;
    private bool playerin = false;
    
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (playerin)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                StartCoroutine(blackScreener());
                player.transform.position = targetDoor.transform.position;
                player.transform.rotation = targetDoor.transform.rotation;
            }
        }

        
    }   

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (needsCrowbar)
            {
                if (CharacterConroller.charinstance.crowbar == false)
                {
                    crowbarNeededMessage.SetActive(true);
                }
                else
                {
                    playerin = true;
                    gears.SetActive(true);
                }

            }

            if (needsGear)
            {
                if (CharacterConroller.charinstance.gear == false)
                {
                    gearNeededMessage.SetActive(true);
                }

                else
                {
                    playerin = true;
                    gears.SetActive(true);
                }

            }

            if (!needsCrowbar && !needsGear)
            {
                playerin = true;
                gears.SetActive(true);
                }
        }

    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerin = false;
            gears.SetActive(false);
            crowbarNeededMessage.SetActive(false);
            gearNeededMessage.SetActive(false);
        }
    }

    IEnumerator blackScreener()

    {
        CharacterConroller.eventhappening = true;
        blackScreen.SetActive(true);
        yield return new WaitForSecondsRealtime(0.5f);
        CharacterConroller.eventhappening = false;
        blackScreen.SetActive(false);
    }

    
}
